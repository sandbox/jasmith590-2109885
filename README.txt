README.txt
==========

A module containing helper functions for Drupal developers and
inquisitive admins. This module allows the specific theme instructions 
to be created for each page/node. Giving the option to choose what stylesheet
and/or page template file to be created inside the given theme.

This module is safe to use on a production site. Just be sure the installed schema
is correct.

BASIC CREATION OF LANDING PAGE THEMES
=========================
1. Determine what can be changed and what will always remain the same.
2. Use base.css as a simple starting point for all css styles.
3. Put all regions/sections of layout into template file so this will be recreated every page
	 regardless of custom page template created. 
	 Examples: Header and Footer can go into html.tpl.php if those are always being repeated and 
	 will never change
4. Try and keep blocks off of the page unless you are sure they will always be repeated.
5. Create page/node in content and set a custom stylesheet to go with that page.
	 This will need to be created after you link it up inside the theme's css folder.
6. Optional -- You can specify a custom page template for that node that will be created inside the
	 current theme selected in the set theme tab in the content creation form.

AUTHOR/MAINTAINER
======================
-Jamie Smith <jasmith590@gmail.com>